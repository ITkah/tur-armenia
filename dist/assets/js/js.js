$(document).ready(function() {

    $(".nav-btn").on("click", function(e){
        e.preventDefault();
				$(".nav-box").fadeToggle(300);
    });

    $(".box-direction-video,.wrapper-video").on("click", function(){
			$(this).addClass("active-video");
				if ($('#video-click').hasClass('active-video')) {
					$(".active-video .play-video").attr('controls', '');
					$(".active-video .play-video").css({"z-index": 3});
					$(this).find("video")[0].play();
				}
    });

    var year = new Date();
		$(".year").html(year.getFullYear());
		
		$('.owl-carousel').owlCarousel({
				dots: true,
				responsive:{
						0:{
								items:1
						},
						1024:{
								items:3
						},
						1280:{
								items:5
						}
				}
		})

		$('.order-gallery-grop').magnificPopup({
			delegate: 'a',
			type: 'image',
			tLoading: 'Loading image #%curr%...',
			mainClass: 'mfp-img-mobile',
			gallery: {
				enabled: true,
				navigateByImgClick: true,
				preload: [0,1]
			},
			image: {
				tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
				titleSrc: function(item) {
					return item.el.attr('title') + '<small>by Marsel Van Oosten</small>';
				}
			}
		});

		new WOW().init();

});
