$(document).ready(function() {
	
	$(".checkboxs-1").click(function(){
		$(".checkboxs-1-text").toggleClass("input-text-active");
	});

	$(".checkboxs-2").click(function(){
		$(".checkboxs-2-text").toggleClass("input-text-active");
	});

    $(".nav-btn").on("click", function (e) {
    	e.preventDefault();
		$(".nav-box").fadeToggle(150);
		if ($(window).width() < 1800) {
			$("body").toggleClass("ovef");
			$("header").toggleClass("header-fixed");
		}
		$(".menu-active-text").toggleClass("active-text-opacity");
    });

    $(".box-direction-video,.wrapper-video").on("click", function () {
    	$(this).addClass("active-video");
    	if ($('#video-click').hasClass('active-video')) {
    		$(".active-video .play-video").attr('controls', '');
    		$(".active-video .play-video").css({
    			"z-index": 3
    		});
    		$(this).find("video")[0].play();
    	}
    });

    var year = new Date();
		$(".year").html(year.getFullYear());
		
		$('.owl-carousel').owlCarousel({
			dots: true,
				responsive: {
						0: {
							items: 1
						},
						1024: {
							items: 3
						},
						1280: {
							items: 5
						}
				}
		})

		$('.order-gallery-grop').magnificPopup({
			delegate: 'a',
			type: 'image',
			tLoading: 'Loading image #%curr%...',
			mainClass: 'mfp-img-mobile',
			gallery: {
				enabled: true,
				navigateByImgClick: true,
				preload: [0,1]
			},
			image: {
				tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
				titleSrc: function(item) {
					return item.el.attr('title') + '<small>by Marsel Van Oosten</small>';
				}
			}
		});

		new WOW().init();
		
		//Код слайдера
		function animateCSS(element, animationName, callback) {
			const node = document.querySelector(element)
			node.classList.add('animated', animationName)
	
			function handleAnimationEnd() {
				node.classList.remove('animated', animationName)
				node.removeEventListener('animationend', handleAnimationEnd)
	
				if (typeof callback === 'function') callback()
			}
	
			node.addEventListener('animationend', handleAnimationEnd)
		}
		
		var tsCurrentSlide;
		var tsNextSlide;
		var tsAfterNextSlide;
		var tsPrevSlide;
		var tsData = [];
		var playing = false;
	
		$(document).ready(function () {
			tsInit();
		});
	
		function tsInit() {
			var elements = $('#tsInitSlider').find('div');
			for (var i = 0; i < elements.length; i++) {
				var tsTitle = $(elements[i]).find('h1').html();
				var tsDescription = $(elements[i]).find('p').html();
				var tsButtonHref = $(elements[i]).find('a').attr('href');
				var tsImageSrc = $(elements[i]).find('img').attr('src');
				tsData.push({
					id: i,
					title: tsTitle,
					description: tsDescription,
					button_src: tsButtonHref,
					image_src: tsImageSrc
				})
			}
			$('.tsInterfaceSlideCurrent').attr('data-slide-id', tsData[0]);


			tsCurrentSlide = tsData[0];
			animateCSS('.tsItemTitleH1', 'fadeInDown');
				$('.tsItemTitleH1').html(tsCurrentSlide.title);
			animateCSS('.tsItemDescription', 'fadeInDown');
				$('.tsItemDescription').find('p').html(tsCurrentSlide.description);
			tsNextSlide = tsData[1];
			tsAfterNextSlide = tsData[2];
			tsPrevSlide = tsData[tsData.length - 1];

			var image_url = 'url(' + tsCurrentSlide.image_src + ')';
			var image_next_url = 'url(' + tsNextSlide.image_src + ')';
			var image_after_next_url = 'url(' + tsAfterNextSlide.image_src + ')';

				$('.tsSlideCurrentItemImageFront').css('background-image', image_url);
				$('.tsSlideCurrentItemImageBack').css('background-image', image_next_url);
				$('.tsSlideCurrentItemImageFront').attr('data-active', '1');
				$('.tsSlideCurrentItemImageBack').attr('data-active', '0');
				$('.tsSlideNextItemImageFront').css('background-image', image_next_url);
				$('.tsSlideNextItemImageBack').css('background-image', image_after_next_url);
				$('.tsSlideNextItemImageFront').attr('data-active', '1');
				$('.tsSlideNextItemImageBack').attr('data-active', '0');
			
			$('.tsInterfaceSlideNext').click(function (e) {
				if (playing)
					return;

				playing = true;
				tsPrevSlide = tsCurrentSlide;
				if (tsCurrentSlide.id == tsData.length - 1) {
					tsCurrentSlide = tsData[0];
				} else
					tsCurrentSlide = tsData[tsCurrentSlide.id + 1];

				if (tsCurrentSlide.id == tsData.length - 1) {
					tsNextSlide = tsData[0];
				} else
					tsNextSlide = tsData[tsCurrentSlide.id + 1];

				if (tsNextSlide.id == tsData.length - 1) {
					tsAfterNextSlide = tsData[0];
				} else
					tsAfterNextSlide = tsData[tsNextSlide.id + 1];
				$('.tsInterfaceSlideCurrent').html('0' + (tsCurrentSlide.id + 1));
				animateCSS('.tsItemTitleH1', 'fadeInDown');
				$('.tsItemTitleH1').html(tsCurrentSlide.title);
				animateCSS('.tsItemDescription', 'fadeInDown');
				$('.tsItemDescription').find('p').html(tsCurrentSlide.description);
				var image_prev_url = 'url(' + tsPrevSlide.image_src + ')';
				var image_url = 'url(' + tsCurrentSlide.image_src + ')';
				var image_next_url = 'url(' + tsNextSlide.image_src + ')';
				var image_after_next_url = 'url(' + tsAfterNextSlide.image_src + ')';

				console.log('\nimage_prev_url:' + image_prev_url);
				console.log('image_url:' + image_url);
				console.log('image_next_url:' + image_next_url);
				console.log('image_after_next_url:' + image_after_next_url);
				anime({
					targets: '.tsSlideCurrentItemImages',
					scale: [{
						value: 1
					}, {
						value: 1.1
					}, {
						value: 1,
						delay: 500
					}],
					rotateY: {
						value: '+=180',
						delay: 400
					},
					easing: 'easeInOutSine',
					duration: 360,
					begin: function (anim) {},
					complete: function (anim) {
						if ($('.tsSlideCurrentItemImageFront').attr('data-active') == '1') {
							$('.tsSlideCurrentItemImageFront').css('background-image', image_next_url);
							$('.tsSlideCurrentItemImageBack').css('background-image', image_url);
							$('.tsSlideCurrentItemImageFront').attr('data-active', '0');
							$('.tsSlideCurrentItemImageBack').attr('data-active', '1');
						} else {
							$('.tsSlideCurrentItemImageFront').css('background-image', image_url);
							$('.tsSlideCurrentItemImageBack').css('background-image', image_next_url);
							$('.tsSlideCurrentItemImageBack').attr('data-active', '0');
							$('.tsSlideCurrentItemImageFront').attr('data-active', '1');
						}
						playing = false;
					}
				});
				anime({
					targets: '.tsSlideNextItemImages',
					scale: [{
						value: 1
					}, {
						value: 1.1
					}, {
						value: 1,
						delay: 500
					}],
					rotateY: {
						value: '+=180',
						delay: 400
					},
					easing: 'easeInOutSine',
					duration: 360,
					complete: function (anim) {
						if ($('.tsSlideNextItemImageFront').attr('data-active') == '1') {
							$('.tsSlideNextItemImageFront').css('background-image', image_after_next_url);
							$('.tsSlideNextItemImageBack').css('background-image', image_next_url);
							$('.tsSlideNextItemImageFront').attr('data-active', '0');
							$('.tsSlideNextItemImageBack').attr('data-active', '1');
						} else {
							$('.tsSlideNextItemImageFront').css('background-image', image_next_url);
							$('.tsSlideNextItemImageBack').css('background-image', image_after_next_url);
							$('.tsSlideNextItemImageBack').attr('data-active', '0');
							$('.tsSlideNextItemImageFront').attr('data-active', '1');
						}
						playing = false;
					}
				});
			});
			$('.tsInterfaceSlidePrev').click(function (e) {
				if (playing)
					return;

				playing = true;
				tsPrevSlide = tsCurrentSlide;
				if (tsCurrentSlide.id == 0) {
					tsCurrentSlide = tsData[tsData.length - 1];
				} else
					tsCurrentSlide = tsData[tsCurrentSlide.id - 1];

				if (tsCurrentSlide.id == 0) {
					tsNextSlide = tsData[tsData.length - 1];
				} else
					tsNextSlide = tsData[tsCurrentSlide.id - 1];

				if (tsNextSlide.id == 0) {
					tsAfterNextSlide = tsData[tsData.length - 1];
				} else
					tsAfterNextSlide = tsData[tsNextSlide.id - 1];
				$('.tsInterfaceSlideCurrent').html('0' + (tsCurrentSlide.id + 1));
				animateCSS('.tsItemTitleH1', 'fadeInDown');
				$('.tsItemTitleH1').html(tsCurrentSlide.title);
				animateCSS('.tsItemDescription', 'fadeInDown');
				$('.tsItemDescription').find('p').html(tsCurrentSlide.description);
				var image_prev_url = 'url(' + tsPrevSlide.image_src + ')';
				var image_url = 'url(' + tsCurrentSlide.image_src + ')';
				var image_next_url = 'url(' + tsNextSlide.image_src + ')';
				var image_after_next_url = 'url(' + tsAfterNextSlide.image_src + ')';
				console.log('\nimage_prev_url:' + image_prev_url);
				console.log('image_url:' + image_url);
				console.log('image_next_url:' + image_next_url);
				console.log('image_after_next_url:' + image_after_next_url);
				anime({
					targets: '.tsSlideCurrentItemImages',
					scale: [{
						value: 1
					}, {
						value: 1.1
					}, {
						value: 1,
						delay: 500
					}],
					rotateY: {
						value: '-=180',
						delay: 400
					},
					easing: 'easeInOutSine',
					duration: 360,
					begin: function (anim) {

					},
					complete: function (anim) {
						if ($('.tsSlideCurrentItemImageFront').attr('data-active') == '1') {
							$('.tsSlideCurrentItemImageFront').css('background-image', image_next_url);
							$('.tsSlideCurrentItemImageBack').css('background-image', image_url);
							$('.tsSlideCurrentItemImageFront').attr('data-active', '0');
							$('.tsSlideCurrentItemImageBack').attr('data-active', '1');
						} else {
							$('.tsSlideCurrentItemImageFront').css('background-image', image_url);
							$('.tsSlideCurrentItemImageBack').css('background-image', image_next_url);
							$('.tsSlideCurrentItemImageBack').attr('data-active', '0');
							$('.tsSlideCurrentItemImageFront').attr('data-active', '1');
						}
						playing = false;
					}
				});
				anime({
					targets: '.tsSlideNextItemImages',
					scale: [{
						value: 1
					}, {
						value: 1.1
					}, {
						value: 1,
						delay: 500
					}],
					rotateY: {
						value: '-=180',
						delay: 400
					},
					easing: 'easeInOutSine',
					duration: 360,
					complete: function (anim) {
						if ($('.tsSlideNextItemImageFront').attr('data-active') == '1') {
							$('.tsSlideNextItemImageFront').css('background-image', image_after_next_url);
							$('.tsSlideNextItemImageBack').css('background-image', image_next_url);
							$('.tsSlideNextItemImageFront').attr('data-active', '0');
							$('.tsSlideNextItemImageBack').attr('data-active', '1');
						} else {
							$('.tsSlideNextItemImageFront').css('background-image', image_next_url);
							$('.tsSlideNextItemImageBack').css('background-image', image_after_next_url);
							$('.tsSlideNextItemImageBack').attr('data-active', '0');
							$('.tsSlideNextItemImageFront').attr('data-active', '1');
						}
						playing = false;
					}
				});
			});
		}

		function animateCSS(element, animationName, callback) {
        const node = document.querySelector(element)
        node.classList.add('animated', animationName)

        function handleAnimationEnd() {
            node.classList.remove('animated', animationName)
            node.removeEventListener('animationend', handleAnimationEnd)

            if (typeof callback === 'function') callback()
        }

        node.addEventListener('animationend', handleAnimationEnd)
    }

	$('html body').on('click','.dots-default', function(){
		$('.dots-default').removeClass('active');
		$(this).addClass('active');
	});

});
