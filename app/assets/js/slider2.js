    function animateCSS(element, animationName, callback) {
        const node = document.querySelector(element)
        node.classList.add('animated', animationName)

        function handleAnimationEnd() {
            node.classList.remove('animated', animationName)
            node.removeEventListener('animationend', handleAnimationEnd)

            if (typeof callback === 'function') callback()
        }

        node.addEventListener('animationend', handleAnimationEnd)
    }

    $(document).ready(function () {
        tsInit2();
    });
    var ts2CurrentSlide2;
    var ts2NextSlide2;
    var ts2AfterNextSlide2;
    var ts2PrevSlide2;
    var ts2Data = [];
    var playing2 = false;

    function tsInit2() {
        var elements = $('#tsInitSlider2').find('div');
        for (var i = 0; i < elements.length; i++) {
            var tsTitle = $(elements[i]).find('h1').html();
            var tsDescription = $(elements[i]).find('p').html();
            var tsButtonHref = $(elements[i]).find('a').attr('href');
            var tsImageSrc = $(elements[i]).find('img').attr('src');
            var tsImageSlide2rSrc = $(elements[i]).find('span').find('img');
            var tsImageSlide2rSrcData = [];
            for (var j = 0; j < tsImageSlide2rSrc.length; j++) {
                tsImageSlide2rSrcData.push('url(' + $(tsImageSlide2rSrc[j]).attr('src') + ')');
            }
            /*console.log(tsImageSlide2rSrcData);
            console.log(tsImageSlide2rSrc);*/
            ts2Data.push({
                id: i,
                title: tsTitle,
                description: tsDescription,
                button_src: tsButtonHref,
                image_src: tsImageSrc,
                image_slider_src: tsImageSlide2rSrcData
            })
        }
        console.log(ts2Data);
        ts2CurrentSlide2 = ts2Data[0];

        //$('.tsItemDescription').find('p').html(ts2CurrentSlide2.description);
        ts2NextSlide2 = ts2Data[1];
        ts2PrevSlide2 = ts2Data[ts2Data.length - 1];

        animateCSS('.tsCurrent', 'fadeInLeft');
        $('.tsLogo').html(ts2CurrentSlide2.title);
        $('.tsDescription').html(ts2CurrentSlide2.description);
        //$('.tsItemTitleH1').html(ts2CurrentSlide2.title);
        animateCSS('.tsPrev', 'fadeInDown');
        $('.tsPrev').html(ts2PrevSlide2.title);
        animateCSS('.tsNext', 'fadeInUp');
        $('.tsNext').html(ts2NextSlide2.title);
        $('.tsDigitSlide2rInfo').html('0' + (ts2CurrentSlide2.id + 1));
        var image_current_url = 'url(' + ts2CurrentSlide2.image_src + ')';
        $('.slider-2').css('background-image', image_current_url);
        $('.tsSlide2CurrentItemImageFront').attr('data-active', '1');
        $('.tsSlide2CurrentItemImageBack').attr('data-active', '0');
        $('.tsImage1').find('.ts2_front').css('background-image', ts2CurrentSlide2.image_slider_src[0]);
        $('.tsImage2').find('.ts2_front_small').css('background-image', ts2CurrentSlide2.image_slider_src[1]);
        $('.tsImage3').find('.ts2_front_small').css('background-image', ts2CurrentSlide2.image_slider_src[2]);
        console.log('prev slide:');
        console.log(ts2PrevSlide2);
        console.log('current slide:');
        console.log(ts2CurrentSlide2);
        console.log('next slide:');
        console.log(ts2NextSlide2);
        $('.tsNext').click(function (e) {
            if (playing2)
                return;
            playing2 = true;

            ts2PrevSlide2 = ts2CurrentSlide2;
            if (ts2CurrentSlide2.id == ts2Data.length - 1) {
                ts2CurrentSlide2 = ts2Data[0];
            } else
                ts2CurrentSlide2 = ts2Data[ts2CurrentSlide2.id + 1];

            if (ts2CurrentSlide2.id == ts2Data.length - 1) {
                ts2NextSlide2 = ts2Data[0];
            } else
                ts2NextSlide2 = ts2Data[ts2CurrentSlide2.id + 1];
            animateCSS('.tsCurrent', 'fadeInLeft');
            $('.tsLogo').html(ts2CurrentSlide2.title);
            $('.tsDescription').html(ts2CurrentSlide2.description);
            //$('.tsItemTitleH1').html(ts2CurrentSlide2.title);
            animateCSS('.tsPrev', 'fadeInDown');
            $('.tsPrev').html(ts2PrevSlide2.title);
            animateCSS('.tsNext', 'fadeInUp');
            $('.tsNext').html(ts2NextSlide2.title);
            animateCSS('.tsDigitSlide2rInfo', 'heartBeat');
            $('.tsDigitSlide2rInfo').html('0' + (ts2CurrentSlide2.id + 1));
            var image_next_url = 'url(' + ts2CurrentSlide2.image_src + ')';
            $('.slider-2').css('background-image', image_next_url);
            //animateCSS('.tsLogo', 'fadeInDown');
            /*animateCSS('.tsCurrent', 'fadeInLeft');
            animateCSS('.tsPrev', 'fadeInDown');*/
            anime({
                targets: '.flipped',
                scale: [{
                    value: 1
                }, {
                    value: 1.1
                }, {
                    value: 1,
                    delay: 600
                }],
                rotateY: {
                    value: '+=180',
                    delay: 600
                },
                easing: 'easeInOutSine',
                duration: 600,
                begin: function (anim) {
                    /*$('.tsSlide2CurrentItemImageFront').attr('data-active', '1');
                    $('.tsSlide2CurrentItemImageBack').attr('data-active', '0');*/
                    if ($('.tsSlide2CurrentItemImageFront').attr('data-active') == '1') {
                        $('.tsImage1').find('.ts2_back').css('background-image', ts2CurrentSlide2.image_slider_src[0]);
                        $('.tsImage2').find('.ts2_back_small').css('background-image', ts2CurrentSlide2.image_slider_src[1]);
                        $('.tsImage3').find('.ts2_back_small').css('background-image', ts2CurrentSlide2.image_slider_src[2]);
                        $('.tsSlide2CurrentItemImageFront').attr('data-active', '0');
                        $('.tsSlide2CurrentItemImageBack').attr('data-active', '1');
                    } else {
                        $('.tsImage1').find('.ts2_front').css('background-image', ts2CurrentSlide2.image_slider_src[0]);
                        $('.tsImage2').find('.ts2_front_small').css('background-image', ts2CurrentSlide2.image_slider_src[1]);
                        $('.tsImage3').find('.ts2_front_small').css('background-image', ts2CurrentSlide2.image_slider_src[2]);
                        $('.tsSlide2CurrentItemImageBack').attr('data-active', '0');
                        $('.tsSlide2CurrentItemImageFront').attr('data-active', '1');
                    }
                },
                complete: function (anim) {

                    playing2 = false;
                }
            });
        });
        $('.tsPrev').click(function (e) {
            if (playing2)
                return;
            playing2 = true;

            ts2PrevSlide2 = ts2CurrentSlide2;
            if (ts2CurrentSlide2.id == 0) {
                ts2CurrentSlide2 = ts2Data[ts2Data.length - 1];
            } else
                ts2CurrentSlide2 = ts2Data[ts2CurrentSlide2.id - 1];

            if (ts2CurrentSlide2.id == 0) {
                ts2NextSlide2 = ts2Data[ts2Data.length - 1];
            } else
                ts2NextSlide2 = ts2Data[ts2CurrentSlide2.id - 1];
            animateCSS('.tsCurrent', 'fadeInLeft');
            $('.tsLogo').html(ts2CurrentSlide2.title);
            $('.tsDescription').html(ts2CurrentSlide2.description);
            //$('.tsItemTitleH1').html(ts2CurrentSlide2.title);
            animateCSS('.tsPrev', 'fadeInDown');
            $('.tsPrev').html(ts2NextSlide2.title);
            animateCSS('.tsNext', 'fadeInUp');
            $('.tsNext').html(ts2PrevSlide2.title);
            animateCSS('.tsDigitSlide2rInfo', 'heartBeat');
            $('.tsDigitSlide2rInfo').html('0' + (ts2CurrentSlide2.id + 1));
            var image_next_url = 'url(' + ts2CurrentSlide2.image_src + ')';
            $('.slider-2').css('background-image', image_next_url);
            //animateCSS('.tsLogo', 'fadeInDown');
            /*animateCSS('.tsCurrent', 'fadeInLeft');
            animateCSS('.tsPrev', 'fadeInDown');*/
            anime({
                targets: '.flipped',
                scale: [{
                    value: 1
                }, {
                    value: 1.1
                }, {
                    value: 1,
                    delay: 600
                }],
                rotateY: {
                    value: '+=180',
                    delay: 600
                },
                easing: 'easeInOutSine',
                duration: 600,
                begin: function (anim) {
                    /*$('.tsSlide2CurrentItemImageFront').attr('data-active', '1');
                    $('.tsSlide2CurrentItemImageBack').attr('data-active', '0');*/
                    if ($('.tsSlide2CurrentItemImageFront').attr('data-active') == '1') {
                        $('.tsImage1').find('.ts2_back').css('background-image', ts2CurrentSlide2.image_slider_src[0]);
                        $('.tsImage2').find('.ts2_back_small').css('background-image', ts2CurrentSlide2.image_slider_src[1]);
                        $('.tsImage3').find('.ts2_back_small').css('background-image', ts2CurrentSlide2.image_slider_src[2]);
                        $('.tsSlide2CurrentItemImageFront').attr('data-active', '0');
                        $('.tsSlide2CurrentItemImageBack').attr('data-active', '1');
                    } else {
                        $('.tsImage1').find('.ts2_front').css('background-image', ts2CurrentSlide2.image_slider_src[0]);
                        $('.tsImage2').find('.ts2_front_small').css('background-image', ts2CurrentSlide2.image_slider_src[1]);
                        $('.tsImage3').find('.ts2_front_small').css('background-image', ts2CurrentSlide2.image_slider_src[2]);
                        $('.tsSlide2CurrentItemImageBack').attr('data-active', '0');
                        $('.tsSlide2CurrentItemImageFront').attr('data-active', '1');
                    }
                },
                complete: function (anim) {

                    playing2 = false;
                }
            });
        });
    }